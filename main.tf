terraform {
  backend "http" {
  }
}
provider "google" {

    credentials = ("creds.json")
  
    project = "communaute-349410"
    region  = "us-central1"
    zone    = "us-central1-c"
}

resource "google_compute_instance" "communaute1" {

    name         = "communaute1"
    machine_type = "f1-micro"

    boot_disk {
      initialize_params {
        image = "debian-cloud/debian-9"
      }
    }

    network_interface {
      network = "default"

      access_config {
        
      }
    }
}